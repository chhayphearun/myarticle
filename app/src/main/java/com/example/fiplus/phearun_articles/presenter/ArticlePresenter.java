package com.example.fiplus.phearun_articles.presenter;

import com.example.fiplus.phearun_articles.entities.PostArticle;

import java.io.File;

public interface ArticlePresenter {
    void onGetAllArticle();
    void onUploadArticle(PostArticle article);
    void onGetAllCategory();
    void onRemoveArticle(int id,int position);
    void onUploadImage(File imageFilePath,int type);
    void onUpdateArticle(long id,PostArticle article);
}
