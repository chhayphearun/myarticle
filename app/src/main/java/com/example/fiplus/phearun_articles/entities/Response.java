package com.example.fiplus.phearun_articles.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response implements Parcelable {
    @Expose
    @SerializedName("DATA")
    private Article Article;
    @Expose
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @Expose
    @SerializedName("CODE")
    private String CODE;
    public Response(){}
    protected Response(Parcel in) {
        Article = in.readParcelable(com.example.fiplus.phearun_articles.entities.Article.class.getClassLoader());
        MESSAGE = in.readString();
        CODE = in.readString();
    }

    public static final Creator<Response> CREATOR = new Creator<Response>() {
        @Override
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        @Override
        public Response[] newArray(int size) {
            return new Response[size];
        }
    };

    public com.example.fiplus.phearun_articles.entities.Article getArticle() {
        return Article;
    }

    public void setArticle(com.example.fiplus.phearun_articles.entities.Article article) {
        Article = article;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(Article, i);
        parcel.writeString(MESSAGE);
        parcel.writeString(CODE);
    }
}
