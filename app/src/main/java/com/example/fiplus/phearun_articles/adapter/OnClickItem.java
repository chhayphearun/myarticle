package com.example.fiplus.phearun_articles.adapter;

import android.view.View;

import com.example.fiplus.phearun_articles.entities.Article;

public interface OnClickItem {
    void onArticleClicked(Article article);
    void onSettingClicked(View view,int position);
}
