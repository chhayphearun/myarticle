package com.example.fiplus.phearun_articles.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fiplus.phearun_articles.R;

public class BottomDialog extends BottomSheetDialogFragment {
    private DialogOnClick dialogOnClick;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dialogOnClick = (DialogOnClick)context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomdialog,container,false);
        TextView gallery = view.findViewById(R.id.gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                dialogOnClick.onDialogItemPick(0);
            }
        });
        TextView takePic = view.findViewById(R.id.takePic);
        takePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                dialogOnClick.onDialogItemPick(1);
            }
        });
        return view;
    }

}
