package com.example.fiplus.phearun_articles.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> implements Filterable {
    private List<Article> articleList;
    private List<Article> fullArticle;
    private OnClickItem OnClickItem;
    private Context context;
    private ResultFilterCallBack resultFilterCallBack;
    public ArticleAdapter(Context context, List<Article> articleList) {
        this.context =context;
        this.articleList = articleList;
        this.fullArticle = articleList;
        if(context instanceof OnClickItem)
        OnClickItem = (OnClickItem)context;
        if(context instanceof ResultFilterCallBack)
            resultFilterCallBack = (ResultFilterCallBack)context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_list,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Article article = articleList.get(i);
        viewHolder.title.setText(article.getTITLE());
        viewHolder.category.setText(article.getCategory().getNAME());
        Glide.with(context)
                .load(article.getIMAGE())
                .apply(new RequestOptions()
                .placeholder(R.drawable.ic_launcher_foreground))
                .into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                int count = fullArticle.size();
                List<Article> filterResult = new ArrayList<>(count);
                if(charSequence!=null){
                    String filter = charSequence.toString().toLowerCase();
                    if(filter.equals("all")){
                        filterResult = fullArticle;
                    }else{
                        String filterable;
                        for(int i = 0;i< count;i++){
                            filterable = fullArticle.get(i).getCategory().getNAME();
                            if(filterable==null){
                                filterable="";
                            }
                            if(filterable.toLowerCase().equals(filter)){
                                filterResult.add(fullArticle.get(i));
                            }
                        }
                    }

                    results.values = filterResult;
                    results.count = filterResult.size();
                }else{
                    results.values = filterResult;
                    results.count = filterResult.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                articleList = (List<Article>) filterResults.values;
                notifyDataSetChanged();
                resultFilterCallBack.onDataChangeListener(filterResults.count);
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView title;
        private TextView category;
        private ImageView setting;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            category = itemView.findViewById(R.id.category);
            setting = itemView.findViewById(R.id.setting);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickItem.onArticleClicked(articleList.get(getAdapterPosition()));
                }
            });
            setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OnClickItem.onSettingClicked(view,getAdapterPosition());
                }
            });
        }
    }
}
