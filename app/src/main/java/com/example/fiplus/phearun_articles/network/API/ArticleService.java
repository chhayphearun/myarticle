package com.example.fiplus.phearun_articles.network.API;

import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.entities.CategoryResponse;
import com.example.fiplus.phearun_articles.entities.ImageResponse;
import com.example.fiplus.phearun_articles.entities.PostArticle;
import com.example.fiplus.phearun_articles.entities.ResponseArticle;
import com.example.fiplus.phearun_articles.entities.Response;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ArticleService {
    @GET("/v1/api/articles")
    Observable<ResponseArticle> getAllArticle();

    @POST("/v1/api/articles")
    Observable<Response> uploadArticle(@Body PostArticle response);

    @DELETE("/v1/api/articles/{id}")
    Observable<Response> removeArticle(@Path("id") long id);

    @GET("/v1/api/articles/{id}")
    Observable<ResponseArticle> getArticleById(@Path("id") long id);

    @PUT("/v1/api/articles/{id}")
    Observable<Response> updateArticle(@Path("id") long id, @Body PostArticle article);

    @GET("/v1/api/categories")
    Observable<CategoryResponse> getAllCate();

    @Multipart
    @POST("/v1/api/uploadfile/single")
    Observable<ImageResponse> uploadImage(@Part MultipartBody.Part file, @Part("FILE")RequestBody name);
}
