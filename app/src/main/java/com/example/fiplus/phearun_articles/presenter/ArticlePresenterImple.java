package com.example.fiplus.phearun_articles.presenter;

import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.entities.Category;
import com.example.fiplus.phearun_articles.entities.PostArticle;
import com.example.fiplus.phearun_articles.entities.Response;
import com.example.fiplus.phearun_articles.entities.ResponseArticle;
import com.example.fiplus.phearun_articles.model.ArticleModel;
import com.example.fiplus.phearun_articles.model.ArticleModelImple;
import com.example.fiplus.phearun_articles.view.AddArticleView;
import com.example.fiplus.phearun_articles.view.ArticleView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ArticlePresenterImple implements ArticlePresenter,ArticleModel.onGetArticleFinishListener,
ArticleModel.onUploadArticleFinishListener,ArticleModel.onGetCategoryFinishListener,
        ArticleModel.onRemoveArticleFinishListener,ArticleModel.onUploadImageFinishListener{
    private ArticleModel articleModel;
    private ArticleView articleView;
    private AddArticleView addArticleView;
    public ArticlePresenterImple(ArticleView articleView) {
        this.articleView = articleView;
        articleModel = new ArticleModelImple();
    }
    public ArticlePresenterImple(AddArticleView addArticleView) {
        this.addArticleView = addArticleView;
        articleModel = new ArticleModelImple();
    }
    @Override
    public void onSuccess(List<Article> articleList) {
        articleView.onActionSuccess(articleList);
    }

    @Override
    public void onSuccess(Response responseArticle) {
        addArticleView.onActionSuccess(responseArticle);
    }

    @Override
    public void onSuccess(ArrayList<Category> categories) {
        addArticleView.onActionSuccess(categories);
    }

    @Override
    public void onSuccess(int position) {
        articleView.onActionSuccess(position);
    }

    @Override
    public void onSuccess(String ImageUrl,int type) {
        addArticleView.onActionSuccess(ImageUrl,type);
    }

    @Override
    public void onError(String error) {
        if(articleView!=null)
        articleView.onError(error);
        if(addArticleView!=null)
        addArticleView.onError(error);
    }

    @Override
    public void onErrorNetwork() {
        articleView.onErrorNetwork();
    }

    @Override
    public void onGetAllArticle() {
        articleModel.getAllArticle(this);
    }

    @Override
    public void onUploadArticle(PostArticle article) {
        articleModel.uploadArticle(article,this);
    }

    @Override
    public void onGetAllCategory() {
        articleModel.getAllCategory(this);
    }

    @Override
    public void onRemoveArticle(int id,int position) {
        articleModel.removeArticle(id,position,this);
    }

    @Override
    public void onUploadImage(File imageFilePath,int type) {
        articleModel.uploadImage(imageFilePath,type,this);
    }

    @Override
    public void onUpdateArticle(long id,PostArticle article) {
        articleModel.updateArticle(id,article,this);
    }
}
