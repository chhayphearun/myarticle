package com.example.fiplus.phearun_articles.view;

import com.example.fiplus.phearun_articles.entities.Article;

import java.util.List;

public interface ArticleView {
    void onError(String error);
    void onErrorNetwork();
    void onActionSuccess(List<Article> articleList);
    void onActionSuccess(int position);

}
