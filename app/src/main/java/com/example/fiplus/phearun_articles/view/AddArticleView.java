package com.example.fiplus.phearun_articles.view;

import com.example.fiplus.phearun_articles.entities.Category;
import com.example.fiplus.phearun_articles.entities.Response;
import com.example.fiplus.phearun_articles.entities.ResponseArticle;

import java.util.ArrayList;

public interface AddArticleView {
    void onError(String message);
    void onValidate(String message);
    void onActionSuccess(Response responseArticle);
    void onActionSuccess(ArrayList<Category> categories);
    void onActionSuccess(String imageUrl,int type);
}
