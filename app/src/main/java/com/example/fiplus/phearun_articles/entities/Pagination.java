package com.example.fiplus.phearun_articles.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pagination {
    @Expose
    @SerializedName("TOTAL_PAGES")
    private int TOTAL_PAGES;
    @Expose
    @SerializedName("TOTAL_COUNT")
    private int TOTAL_COUNT;
    @Expose
    @SerializedName("LIMIT")
    private int LIMIT;
    @Expose
    @SerializedName("PAGE")
    private int PAGE;

    public int getTOTAL_PAGES() {
        return TOTAL_PAGES;
    }

    public void setTOTAL_PAGES(int TOTAL_PAGES) {
        this.TOTAL_PAGES = TOTAL_PAGES;
    }

    public int getTOTAL_COUNT() {
        return TOTAL_COUNT;
    }

    public void setTOTAL_COUNT(int TOTAL_COUNT) {
        this.TOTAL_COUNT = TOTAL_COUNT;
    }

    public int getLIMIT() {
        return LIMIT;
    }

    public void setLIMIT(int LIMIT) {
        this.LIMIT = LIMIT;
    }

    public int getPAGE() {
        return PAGE;
    }

    public void setPAGE(int PAGE) {
        this.PAGE = PAGE;
    }
}
