package com.example.fiplus.phearun_articles;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.fiplus.phearun_articles.dialog.BottomDialog;
import com.example.fiplus.phearun_articles.dialog.DialogOnClick;
import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.entities.Category;
import com.example.fiplus.phearun_articles.entities.PostArticle;
import com.example.fiplus.phearun_articles.entities.Response;
import com.example.fiplus.phearun_articles.presenter.ArticlePresenter;
import com.example.fiplus.phearun_articles.presenter.ArticlePresenterImple;
import com.example.fiplus.phearun_articles.view.AddArticleView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddArticleActivity extends AppCompatActivity implements AddArticleView,DialogOnClick {
    private ArticlePresenter presenter;
    private Spinner spinner;
    private ArrayList<Category> categories;
    private int cateId=0;
    private EditText title,desc;
    private ImageView pickImage;
    private File file;
    private String imageFilePath;
    public static final int CAMERA_REQUEST = 10;
    public static final int ADD = 1;
    public static final int UPDATE = 2;
    public static final int RequestPermissionCode = 11;
    private int TYPE;
    private PostArticle postArticle;
    private String imageUrl;
    private Article article;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);
        spinner = findViewById(R.id.spinner);
        title = findViewById(R.id.title);
        desc = findViewById(R.id.description);
        Button bt = findViewById(R.id.addNew);
        pickImage = findViewById(R.id.pickImage);
        progressBar = findViewById(R.id.progressBar);
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            TYPE = bundle.getInt("type",0);
            if(TYPE==ADD){
                bt.setText("Add");
                setToolBarTitle("Add Article");
            }else if(TYPE==UPDATE){
                bt.setText("Update");
                setToolBarTitle("Update Article");
                article = bundle.getParcelable("article");
                if(article!=null){
                    title.setText(article.getTITLE());
                    desc.setText(article.getDESCRIPTION());
                    Glide.with(this)
                            .load(article.getIMAGE())
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.ic_launcher_foreground))
                            .into(pickImage);
                    int i = article.getCategory().getID();
                    spinner.setSelection(i);
                    imageUrl = article.getIMAGE();
                }
            }
        }
        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkPermission()){
                    openBottomDialog();
                }else{
                    requestPermission();
                }


            }
        });
        categories = new ArrayList<>();
        presenter = new ArticlePresenterImple(this);
        presenter.onGetAllCategory();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cateId = categories.get(i).getID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                if(TYPE==ADD){
                    if(file==null){
                        String mTitle = title.getText().toString();
                        String mDesc = desc.getText().toString();
                        postArticle = setArticle(1,cateId,mTitle,mDesc,imageUrl,"1");
                        presenter.onUploadArticle(postArticle);
                    }else{
                        presenter.onUploadImage(file,ADD);
                    }
                }else if(TYPE==UPDATE){
                    if(file!=null){
                        presenter.onUploadImage(file,UPDATE);
                    }else{
                        String mTitle = title.getText().toString();
                        String mDesc = desc.getText().toString();
                        postArticle = setArticle(1,cateId,mTitle,mDesc,imageUrl,"1");
                        presenter.onUpdateArticle(article.getID(), postArticle);
                    }
                }
            }
        });
    }
    private void openBottomDialog(){
        BottomDialog bottomDialog = new BottomDialog();
        FragmentManager fragmentManager = getSupportFragmentManager();
        bottomDialog.show(fragmentManager,"");
    }
    private PostArticle setArticle(int authId,int cateId,String title,String desc,String imageUrl,String status){
        postArticle = new PostArticle();
        postArticle.setAUTHOR(authId);
        postArticle.setCATEGORY_ID(cateId);
        postArticle.setTITLE(title);
        postArticle.setDESCRIPTION(desc);
        postArticle.setIMAGE(imageUrl);
        postArticle.setSTATUS(status);
        return postArticle;
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1 && data!=null){
            Uri uri = data.getData();
            String name = getRealPathFromURI(uri);
            File tempFile = new File(this.getFilesDir().getAbsolutePath(), name);
            //Copy URI contents into temporary file.
            try {
                if(tempFile.createNewFile()){
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(tempFile));
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.close();
                }
            }
            catch (IOException e) {
                //Log Error
                e.printStackTrace();
            }
            //Now fetch the new URI
            file = tempFile;
            Uri newUri = Uri.fromFile(tempFile);
            pickImage.setImageURI(newUri);
        }else if(requestCode == CAMERA_REQUEST){
            if(resultCode== RESULT_OK){
                file = new  File(imageFilePath);
                pickImage.setImageURI(Uri.parse(imageFilePath));
            }
        }else if(requestCode == RESULT_CANCELED){

        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void setToolBarTitle(String textName){
        ActionBar actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.toolbar_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textTitle = viewActionBar.findViewById(R.id.toolbar_title);
        textTitle.setText(textName);

        if(actionBar!=null){
            actionBar.setCustomView(viewActionBar, params);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidate(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActionSuccess(Response responseArticle) {
        progressBar.setVisibility(View.GONE);
        Intent intent = new Intent();
        setResult(RESULT_OK,intent);
        finish();

    }

    @Override
    public void onActionSuccess(ArrayList<Category> categories) {
        this.categories =categories;
        List<String> cateName = new ArrayList<>();
        for(int in =0; in<categories.size();in++){
            cateName.add(categories.get(in).getNAME());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,cateName);
        spinner.setAdapter(adapter);
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]
                {
                        CAMERA,
                        WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE
                }, RequestPermissionCode);
    }
    private boolean checkPermission(){
        int cameraPermission = ContextCompat.checkSelfPermission(getApplicationContext(),CAMERA);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(),WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(),READ_EXTERNAL_STORAGE);
        return cameraPermission == PackageManager.PERMISSION_GRANTED &&
                writeStorage == PackageManager.PERMISSION_GRANTED &&
                readStorage == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case RequestPermissionCode:
                if(grantResults.length>0){
                    boolean Camera = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadExternalStorage = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if(Camera && ReadExternalStorage && WriteExternalStorage){
                        openBottomDialog();
                    }else{
                        Toast.makeText(this, "Permission Denied.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onActionSuccess(String imageUrl,int type) {
        String mTitle = title.getText().toString();
        String mDesc = desc.getText().toString();
        postArticle = setArticle(1,cateId,mTitle,mDesc,imageUrl,"1");
        if(type==ADD){
            presenter.onUploadArticle(postArticle);
        }else if(type==UPDATE){
            presenter.onUpdateArticle(article.getID(), postArticle);
        }



    }
    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile;
                try {
                    photoFile = createImageFile();
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                Uri photoUri = FileProvider.getUriForFile(this, getPackageName() +".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                setResult(RESULT_OK,pictureIntent);
                startActivityForResult(pictureIntent, CAMERA_REQUEST);
            }
    }
    private File createImageFile() throws IOException {
        File image;
            String timeStamp =
                    new SimpleDateFormat("yyyyMMdd_HHmmss",
                            Locale.getDefault()).format(new java.util.Date());
            String imageFileName = "IMG_" + timeStamp + "_";
            File storageDir =
                    getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            imageFilePath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onDialogItemPick(int i) {
        if(i==0){
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent,1);
        }else if(i==1){
            openCameraIntent();
        }
    }
}
