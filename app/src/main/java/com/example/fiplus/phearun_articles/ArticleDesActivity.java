package com.example.fiplus.phearun_articles;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.RequestOptions;
import com.example.fiplus.phearun_articles.entities.Article;

import java.security.MessageDigest;

public class ArticleDesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_des);
        if(getSupportActionBar()!=null)
        getSupportActionBar().hide();
        ImageView imageView = findViewById(R.id.image);
        TextView title = findViewById(R.id.title);
        TextView des = findViewById(R.id.description);
        TextView authorName = findViewById(R.id.author_name);
        TextView date = findViewById(R.id.date);
        ImageButton bt = findViewById(R.id.btclose);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent intent = getIntent();
        Article article = intent.getParcelableExtra("Article");
        title.setText(article.getTITLE());
        des.setText(article.getDESCRIPTION());
        String name = "Phearun";
        if(name!=null){
            authorName.setText("By : "+name);
        }
        date.setText("Date : "+article.getCREATED_DATE().substring(0,8) + ","+article.getCREATED_DATE().substring(8));
        Glide.with(this)
                .load(article.getIMAGE())
                .apply(new RequestOptions()
                .placeholder(R.drawable.ic_launcher_foreground)
                .centerCrop())
                .into(imageView);
    }
}
