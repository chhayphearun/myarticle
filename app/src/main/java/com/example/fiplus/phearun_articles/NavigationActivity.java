package com.example.fiplus.phearun_articles;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fiplus.phearun_articles.adapter.ResultFilterCallBack;
import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.adapter.ArticleAdapter;
import com.example.fiplus.phearun_articles.adapter.OnClickItem;
import com.example.fiplus.phearun_articles.entities.Response;
import com.example.fiplus.phearun_articles.presenter.ArticlePresenter;
import com.example.fiplus.phearun_articles.presenter.ArticlePresenterImple;
import com.example.fiplus.phearun_articles.view.ArticleView;

import java.util.ArrayList;
import java.util.List;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,ArticleView,OnClickItem,ResultFilterCallBack {
    private RecyclerView recyclerView;
    private List<Article> articleList;
    private ArticlePresenter presenter;
    private SwipeRefreshLayout refreshLayout;
    private ArticleAdapter adapter;
    private ProgressBar progressBar;
    private LinearLayout noDataLayout;
    private Button retry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        View view_tool = findViewById(R.id.toolbar);
        Toolbar toolbar = view_tool.findViewById(R.id.toolbar);
        TextView tool_text = toolbar.findViewById(R.id.toolbar_title);
        tool_text.setText("កម្សាន");
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        presenter = new ArticlePresenterImple(this);
        presenter.onGetAllArticle();

//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View view = findViewById(R.id.app_bar);
        View contentView = view.findViewById(R.id.content);
        refreshLayout = view.findViewById(R.id.swipeRefresh);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        noDataLayout = view.findViewById(R.id.noDataLayout);
        retry = view.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retry.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                presenter.onGetAllArticle();
            }
        });
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onGetAllArticle();

            }
        });
        recyclerView = contentView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        articleList = new ArrayList<>();
        if(!articleList.isEmpty()){
            ArticleAdapter adapter = new ArticleAdapter(getApplication(),articleList);
            recyclerView.setAdapter(adapter);
        }
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_sort_black_24dp);
        toolbar.setOverflowIcon(drawable);

//add menu programmatically
//        String cate = "";
//        for (int i =0 ;i<articleList.size();i++){
//            String cateName = articleList.get(i).getCategory().getNAME();
//            if(cateName==null){
//                menu.add(Menu.NONE, Menu.NONE, Menu.NONE, "Other");
//            }else{
//                if(!cateName.equals(cate)){
//                    cate = cateName;
//                    menu.add(Menu.NONE, Menu.NONE, Menu.NONE, cate);
//                }
//            }
//        }
//        menu.add(Menu.NONE, Menu.NONE, Menu.NONE, "23123");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation,menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.news){
            if(adapter!=null)
            adapter.getFilter().filter(item.getTitle());
        }else if(id == R.id.hotNews){
            if(adapter!=null)
            adapter.getFilter().filter(item.getTitle());
        }else if(id == R.id.other){
            if(adapter!=null)
            adapter.getFilter().filter("");
        }else if(id == R.id.all){
            if(adapter!=null)
            adapter.getFilter().filter("all");
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add){
            Intent intent = new Intent(this,AddArticleActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("type", 1);
            intent.putExtras(bundle);
            startActivityForResult(intent,1);
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorNetwork() {
        progressBar.setVisibility(View.GONE);
        retry.setVisibility(View.VISIBLE);
        Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActionSuccess(List<Article> articleList) {
        progressBar.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
        if(articleList.isEmpty()){
            noDataLayout.setVisibility(View.VISIBLE);
        }else{
            noDataLayout.setVisibility(View.GONE);
            this.articleList = articleList;
            adapter = new ArticleAdapter(this,articleList);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onActionSuccess(int position) {
        progressBar.setVisibility(View.GONE);
        articleList.remove(position);
        if(adapter!=null)
            adapter.notifyDataSetChanged();
        if(articleList.size()==0){
            noDataLayout.setVisibility(View.VISIBLE);
        }else{
            noDataLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onArticleClicked(Article article) {
        Intent intent = new Intent(this,ArticleDesActivity.class);
        intent.putExtra("Article",article);
        article.getAuthor().getNAME();
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                presenter.onGetAllArticle();
            }
        }
    }
    @Override
    public void onSettingClicked(View view, final int position) {
        PopupMenu popupMenu = new PopupMenu(this,view);
        popupMenu.getMenuInflater().inflate(R.menu.setting_menu,popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.remove:
                        progressBar.setVisibility(View.VISIBLE);
                        presenter.onRemoveArticle(articleList.get(position).getID(),position);
                        return true;
                    case R.id.update:
                        Intent intent = new Intent(NavigationActivity.this,AddArticleActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("type",2);
                        bundle.putParcelable("article", articleList.get(position));
                        intent.putExtras(bundle);
                        startActivityForResult(intent,1);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDataChangeListener(int count) {
        if(count==0){
            noDataLayout.setVisibility(View.VISIBLE);
        }else{
            noDataLayout.setVisibility(View.GONE);
        }
    }
}
