package com.example.fiplus.phearun_articles.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;

import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.entities.Category;
import com.example.fiplus.phearun_articles.entities.CategoryResponse;
import com.example.fiplus.phearun_articles.entities.ImageResponse;
import com.example.fiplus.phearun_articles.entities.PostArticle;
import com.example.fiplus.phearun_articles.entities.ResponseArticle;
import com.example.fiplus.phearun_articles.entities.Response;
import com.example.fiplus.phearun_articles.network.API.ArticleService;
import com.example.fiplus.phearun_articles.network.service.ServiceGenerators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ArticleModelImple implements ArticleModel {
    private onGetArticleFinishListener listener;
    private onUploadArticleFinishListener uploadArticleListener;
    private onGetCategoryFinishListener getCategoryListener;
    private onRemoveArticleFinishListener removeArticleListener;
    private onUploadImageFinishListener uploadImageListener;
    private ArticleService service;
    private List<Article> articleList;
    private ArrayList<Category> categories;
    private Response response;
    @Override
    public void getAllArticle(onGetArticleFinishListener listener) {
        this.listener = listener;
        callGetAllArticle();
    }

    @Override
    public void uploadArticle(PostArticle article, onUploadArticleFinishListener listener) {
        uploadArticleListener = listener;
        uploadArticle(article);
    }

    @Override
    public void getAllCategory(onGetCategoryFinishListener listener) {
        getCategoryListener = listener;
        getAllCategory();
    }

    @Override
    public void removeArticle(int id,int position,onRemoveArticleFinishListener listener) {
        removeArticleListener = listener;
        removeArticle(id,position);
    }

    @Override
    public void uploadImage(File imageFilePath, int type,onUploadImageFinishListener listener) {
        uploadImageListener = listener;
        uploadImage(imageFilePath,type);
    }

    @Override
    public void updateArticle(long id,PostArticle article, onUploadArticleFinishListener listener) {
        uploadArticleListener = listener;
        onUpdateArticle(id,article);
    }
    private void onUpdateArticle(long id,PostArticle article){
        response = new Response();
        service = ServiceGenerators.createService(ArticleService.class);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(service.updateArticle(id,article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response>() {
                    @Override
                    public void onNext(Response res) {
                        response = res;
                    }

                    @Override
                    public void onError(Throwable e) {
                        uploadArticleListener.onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        uploadArticleListener.onSuccess(response);
                    }
                }));
    }
    private void uploadImage(File imageFile, final int type){
        service = ServiceGenerators.createService(ArticleService.class);
        long length = saveBitmapToFile(imageFile).length();
        Log.i("sizeImage", String.valueOf(length));
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"),imageFile);
        MultipartBody.Part fileUpload = MultipartBody.Part.createFormData("FILE",imageFile.getName(),requestBody);
        RequestBody fileName = RequestBody.create(MediaType.parse("text/plain"),imageFile.getName());
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(service.uploadImage(fileUpload,fileName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(new DisposableObserver<ImageResponse>() {
            @Override
            public void onNext(ImageResponse imageResponse) {
                uploadImageListener.onSuccess(imageResponse.getData(),type);
            }

            @Override
            public void onError(Throwable e) {
                uploadImageListener.onError(e.toString());
            }

            @Override
            public void onComplete() {

            }
        }));

    }
    public File saveBitmapToFile(File file){
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image
            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            // The new size we want to scale to
            final int REQUIRED_SIZE=80;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();
            try {
                ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                Matrix matrix = new Matrix();
                if (orientation == 6) {matrix.postRotate(90);}
                else if (orientation == 3) {matrix.postRotate(180);}
                else if (orientation == 8) {matrix.postRotate(270);}
                assert selectedBitmap != null;
                selectedBitmap = Bitmap.createBitmap(selectedBitmap, 0, 0, selectedBitmap.getWidth(), selectedBitmap.getHeight(), matrix, true); // rotating bitmap
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            assert selectedBitmap != null;
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 90 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }
    private void removeArticle(int id, final int position){
        service = ServiceGenerators.createService(ArticleService.class);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(service.removeArticle(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response>() {
                    @Override
                    public void onNext(Response response)
                    {
                        Log.i("1234567890",response.getMESSAGE());
                    }

                    @Override
                    public void onError(Throwable e) {
                        removeArticleListener.onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        removeArticleListener.onSuccess(position);
                    }
                }));
    }
    private void getAllCategory(){
        categories = new ArrayList<>();
        service = ServiceGenerators.createService(ArticleService.class);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(service.getAllCate()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<CategoryResponse>() {
            @Override
            public void onNext(CategoryResponse categoryResponse) {
                categories.addAll(categoryResponse.getCategories());
            }

            @Override
            public void onError(Throwable e) {
                getCategoryListener.onError(e.toString());
            }

            @Override
            public void onComplete() {
                getCategoryListener.onSuccess(categories);
            }
        }));
    }
    private void uploadArticle(PostArticle article){
        response = new Response();
        service = ServiceGenerators.createService(ArticleService.class);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(service.uploadArticle(article)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<Response>() {
            @Override
            public void onNext(Response res) {
                response = res;
            }

            @Override
            public void onError(Throwable e) {
                uploadArticleListener.onError(e.toString());
            }

            @Override
            public void onComplete() {
                uploadArticleListener.onSuccess(response);
            }
        }));

    }
    private void callGetAllArticle(){
        articleList = new ArrayList<>();
        service = ServiceGenerators.createService(ArticleService.class);
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(service.getAllArticle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseArticle>() {
                    @Override
                    public void onNext(ResponseArticle responseArticle) {
                        articleList.addAll(responseArticle.getArticle());
                    }
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof UnknownHostException){
                            listener.onErrorNetwork();
                        }else{
                            listener.onError(e.toString());
                        }

                    }

                    @Override
                    public void onComplete() {
                        listener.onSuccess(articleList);

                    }
                }));
    }
}
