package com.example.fiplus.phearun_articles.adapter;

public interface ResultFilterCallBack {
    void onDataChangeListener(int count);
}
