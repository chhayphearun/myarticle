package com.example.fiplus.phearun_articles.model;

import com.example.fiplus.phearun_articles.entities.Article;
import com.example.fiplus.phearun_articles.entities.Category;
import com.example.fiplus.phearun_articles.entities.PostArticle;
import com.example.fiplus.phearun_articles.entities.Response;
import com.example.fiplus.phearun_articles.entities.ResponseArticle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public interface ArticleModel {
    interface onGetArticleFinishListener{
        void onSuccess(List<Article> articleList);
        void onError(String error);
        void onErrorNetwork();
    }
    void getAllArticle(onGetArticleFinishListener listener);

    interface onUploadArticleFinishListener{
        void onSuccess(Response responseArticle);
        void onError(String error);
    }
    void uploadArticle(PostArticle article, onUploadArticleFinishListener listener);

    interface onGetCategoryFinishListener{
        void onSuccess(ArrayList<Category> categories);
        void onError(String error);
    }
    void getAllCategory(onGetCategoryFinishListener listener);

    interface onRemoveArticleFinishListener{
        void onSuccess(int position);
        void onError(String error);
    }
    void removeArticle(int id,int position,onRemoveArticleFinishListener listener);

    interface onUploadImageFinishListener{
        void onSuccess(String ImageUrl,int type);
        void onError(String error);
    }
    void uploadImage(File imageFilePath,int type, onUploadImageFinishListener listener);
    void updateArticle(long id,PostArticle article, onUploadArticleFinishListener listener);
}
