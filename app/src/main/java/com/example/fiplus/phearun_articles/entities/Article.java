package com.example.fiplus.phearun_articles.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Article implements Parcelable {
    @Expose
    @SerializedName("IMAGE")
    private String IMAGE;
    @Expose
    @SerializedName("CATEGORY")
    private Category Category;
    @Expose
    @SerializedName("STATUS")
    private String STATUS;
    @Expose
    @SerializedName("AUTHOR")
    private Author Author;
    @Expose
    @SerializedName("CREATED_DATE")
    private String CREATED_DATE;
    @Expose
    @SerializedName("DESCRIPTION")
    private String DESCRIPTION;
    @Expose
    @SerializedName("TITLE")
    private String TITLE;
    @Expose
    @SerializedName("ID")
    private int ID;
    public Article(){

    }


    protected Article(Parcel in) {
        IMAGE = in.readString();
        Category = in.readParcelable(com.example.fiplus.phearun_articles.entities.Category.class.getClassLoader());
        STATUS = in.readString();
        Author = in.readParcelable(com.example.fiplus.phearun_articles.entities.Author.class.getClassLoader());
        CREATED_DATE = in.readString();
        DESCRIPTION = in.readString();
        TITLE = in.readString();
        ID = in.readInt();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public Category getCategory() {
        return Category;
    }

    public void setCategory(Category category) {
        this.Category = category;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public Author getAuthor() {
        return Author;
    }

    public void setAuthor(Author author) {
        this.Author = author;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(IMAGE);
        parcel.writeParcelable(Category, i);
        parcel.writeString(STATUS);
        parcel.writeParcelable(Author, i);
        parcel.writeString(CREATED_DATE);
        parcel.writeString(DESCRIPTION);
        parcel.writeString(TITLE);
        parcel.writeInt(ID);
    }
}
