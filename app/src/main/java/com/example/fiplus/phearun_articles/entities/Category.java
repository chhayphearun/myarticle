package com.example.fiplus.phearun_articles.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category implements Parcelable {
    @Expose
    @SerializedName("NAME")
    private String NAME;
    @Expose
    @SerializedName("ID")
    private int ID;

    public Category(String NAME, int ID) {
        this.NAME = NAME;
        this.ID = ID;
    }

    public Category(){

    }

    protected Category(Parcel in) {
        NAME = in.readString();
        ID = in.readInt();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(NAME);
        parcel.writeInt(ID);
    }
}
